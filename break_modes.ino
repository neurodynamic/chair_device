void execute_break_mode_ticklist()
{
  shade_red_to_blue_fade(break_time_elapsed, break_length);

  if(break_time_elapsed >= break_length){
    change_to_work_mode();
  }
}

void execute_long_break_mode_ticklist()
{
  shade_red_to_blue_fade(break_time_elapsed, long_break_length);

  if(break_time_elapsed >= long_break_length){
    change_to_work_mode();
  }
}

void change_to_work_mode()
{
  reset_timers();
  lifecycle_mode = WORK_MODE;
  shade_green_to_red_fade();
  play_low_pitch_victory(buzzer_pin);
}

void shade_green_to_red_fade()
{
  int red_intensity, blue_intensity = 0, green_intensity, normalized_fade_completion;

  normalized_fade_completion = map(work_time_elapsed + limbo_time_elapsed, 0, work_length + (limbo_length/2), 0, 255);
  normalized_fade_completion = constrain(normalized_fade_completion, 0, 255);

  red_intensity = normalized_fade_completion;
  green_intensity = 255 - normalized_fade_completion;

  write_to_color_led(red_intensity, green_intensity, blue_intensity);
}

void shade_red_to_blue_fade(int fade_completion, int fade_length)
{
  int red_intensity, blue_intensity, green_intensity = 0, normalized_fade_completion;

  normalized_fade_completion = map(fade_completion, 0, fade_length, 0, 255);
  normalized_fade_completion = constrain(normalized_fade_completion, 0, 255);

  blue_intensity = normalized_fade_completion;
  red_intensity = 255 - normalized_fade_completion;

  write_to_color_led(red_intensity, green_intensity, blue_intensity);
}

void write_to_color_led(int red_intensity, int green_intensity, int blue_intensity)
{
  analogWrite(RED_PIN, red_intensity);
  analogWrite(BLUE_PIN, blue_intensity);
  analogWrite(GREEN_PIN, green_intensity);
}
